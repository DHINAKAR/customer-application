import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeModule } from './home/home.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HeaderModule } from './header/header.module';
import { FooterModule } from './footer/footer.module';
import { SharedService } from '../shared/shared.service';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
        AppComponent
],
  imports: [
  FormsModule,
HttpClientModule,
HomeModule,
FooterModule,
HeaderModule,
AppRoutingModule,
BrowserAnimationsModule,
      BrowserModule,
      ToastrModule.forRoot(),
],
  providers: [
    	SharedService
],
  bootstrap: [AppComponent]
})
export class AppModule { }