import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../../shared/shared.service';

@Injectable({
    providedIn: 'root'
})

export class GetallusersService {
    constructor(
        private sharedService: SharedService,
        private http: HttpClient,
    ) { }

    GetAllValues(): Observable<any> {
      return this.http.get(this.sharedService.DESKTOP_API + '/api/customer');
    }
}