import { Component, OnInit } from '@angular/core';
import { GetallusersService } from './getallusers.service';
import {Router} from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-getallusers',
  templateUrl: './getallusers.component.html',
  styleUrls: ['./getallusers.component.scss'],
})

export class GetallusersComponent implements OnInit {
    columnDefs: any = [{ headerName: 'First Name', field: 'firstName'  },
    { headerName: 'Last Name', field: 'lastName'  },
    { headerName: 'Email', field: 'email'  },
    { headerName: 'Phone Number', field: 'phone'  },
    {headerName: 'Action', field: 'action'}];
    public User = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        phone: '',
    };
    paginationPageSize = 5;
 	page = 1;
 	rowData: any = [];

    constructor (
        private getallusersService: GetallusersService,
        private router: Router,
        private SpinnerService: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.GetAllValues();
    }

    createcustomer() {
        this.router.navigate(['/user/create']);
    }

    editcustomer(data) {
        this.router.navigate(['/user'], { queryParams: { id : data.id } });
    }
    GetAllValues() {
        this.SpinnerService.show();
        this.getallusersService.GetAllValues().subscribe(data => {
            this.SpinnerService.hide();
            this.rowData = data;
        },
        error => {
            console.log('Error', error);
        });
    }
}