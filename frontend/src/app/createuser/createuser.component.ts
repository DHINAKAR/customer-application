import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CreateuserService } from './createuser.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'app-createuser',
    templateUrl: './createuser.component.html',
    styleUrls: ['./createuser.component.scss'],
})

export class CreateuserComponent implements OnInit {
    submit = false;
    public createform: FormGroup;
    public show: boolean;

    constructor(
        private createuserService: CreateuserService,
        private router: Router,
        private toastr: ToastrService
    ) {
        this.show = false;
     }

    ngOnInit() {
        this.createform = new FormGroup({
            createdata: new FormGroup({
              firstname: new FormControl(null, [Validators.required , Validators.pattern('(?!-)[a-zA-Z-]*[a-zA-Z]$')]),
              lastname: new FormControl(null, [Validators.required ,Validators.pattern('(?!-)[a-zA-Z-]*[a-zA-Z]$')]),
              email: new FormControl(null, [Validators.required, Validators.email]),
              password: new FormControl(null, [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])[A-Za-z\d].{8,}')]),
              phone: new FormControl(null, [Validators.required]),
            })
          });
    }
    Create() {
        this.submit = true;
        if (this.createform.invalid) {
            return;
        }
        const createinfo = {
            firstName: this.createform.value.createdata.firstname,
            lastName: this.createform.value.createdata.lastname,
            email: this.createform.value.createdata.email,
            password: this.createform.value.createdata.password,
            phone: this.createform.value.createdata.phone
          };
        this.createuserService.Create(createinfo).subscribe(data => {
            if (data) {
                this.toastr.success('Success', 'Created', {
                    closeButton: true,
                });
                this.router.navigate(['users']);
            }
        },
            error => {
                this.toastr.error('Error', 'Something Went Wrong', {
                    closeButton: true,
                });
                console.log('Error', error);
            });
    }

    Cancel() {
        this.router.navigate(['']);
    }
    hideEye() {
        this.show = !this.show;
      }
    
}
