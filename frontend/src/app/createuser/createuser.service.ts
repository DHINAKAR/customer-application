import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../../shared/shared.service';

@Injectable({
    providedIn: 'root'
})

export class CreateuserService {
    constructor(
        private sharedService: SharedService,
        private http: HttpClient,
    ) { }

    Create(User): Observable<any> {
        return this.http.post(this.sharedService.DESKTOP_API + '/api/customer', User);
    }
}
