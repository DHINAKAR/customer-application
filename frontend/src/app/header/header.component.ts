import { Component, OnInit, Inject } from '@angular/core';
import { ITranslationService, I18NEXT_SERVICE } from 'angular-i18next';
import { Router, NavigationEnd } from '@angular/router';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})

export class HeaderComponent implements OnInit {
	private jwtToken: String;
	public isAdminUser = false;
	mysubscription: any;
	public authArray: any;
	public userId: string;
	public currentLanguage: String;
	public confirmLangChangeModal: String = 'none';
	public language = 'en';
	public languages = ['en', 'ta', 'es']
	constructor(
		@Inject(I18NEXT_SERVICE) private i18NextService: ITranslationService,
		private router: Router,
	) {
		this.mysubscription = this.router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				this.router.navigated = false;
			}
		})
		this.router.routeReuseStrategy.shouldReuseRoute = function () {
			return false;
		}
	}

	ngOnInit() {

	}

	onCloseHandled() {
		this.confirmLangChangeModal = 'none';
	}
}