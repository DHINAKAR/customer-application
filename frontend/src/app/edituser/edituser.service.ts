import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../../shared/shared.service';

@Injectable({
    providedIn: 'root'
})

export class EdituserService {
    constructor(
        private sharedService: SharedService,
        private http: HttpClient,
    ) { }

    Delete(id): Observable<any> {
        return this.http.delete(this.sharedService.DESKTOP_API + '/api/customer/' + id );
    }
    Update(User, id): Observable<any> {
        return this.http.put(this.sharedService.DESKTOP_API + '/api/customer/' + `${id}`, User);
    }

    getCustomerById(id): Observable<any> {
        return this.http.get(this.sharedService.DESKTOP_API + `/api/customer/${id}`);
    }
}