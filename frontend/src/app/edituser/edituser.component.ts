import { Component, OnInit } from '@angular/core';
import { EdituserService } from './edituser.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'app-edituser',
    templateUrl: './edituser.component.html',
    styleUrls: ['./edituser.component.scss'],
})

export class EdituserComponent implements OnInit {
    queryId: any;
    public User = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        phone: '',
    };
    submitted = false;

    public editform: FormGroup;
    public show: boolean;

    constructor(
        private edituserService: EdituserService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
        private formBuilder: FormBuilder
    ) {
        this.show = false;
    }

    ngOnInit() {
        this.editform = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.pattern('(?!-)[a-zA-Z-]*[a-zA-Z]$')]],
            lastName: ['', [Validators.required, Validators.pattern('(?!-)[a-zA-Z-]*[a-zA-Z]$')]],
            phone: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])[A-Za-z\d].{8,}')]],
        }
        );
        this.activatedRoute.queryParams.subscribe(params => {
            if (params.id) {
                this.queryId = params.id;
                this.edituserService.getCustomerById(params.id).subscribe(data => {
                    if (data) {
                        this.editform.controls.firstName.setValue(data.firstName);
                        this.editform.controls.lastName.setValue(data.lastName);
                        this.editform.controls.email.setValue(data.email);
                        this.editform.controls.password.setValue(data.password);
                        this.editform.controls.phone.setValue(data.phone);
                    }
                });
            }
        });
    }

    get f(): any {
        return this.editform.controls;
    }

    delete() {
        this.edituserService.Delete(this.queryId).subscribe(data => {
            if (data) {
                this.toastr.success('Success', 'Deleted', {
                    closeButton: true,
                });
                this.router.navigate(['users']);
            }
        },
            error => {
                this.toastr.error('Error', 'Something Went Wrong', {
                    closeButton: true,
                });
                console.log('Error', error);
            });
    }

    onSubmit() {
        this.submitted = true;
        if (this.editform.invalid) {
            return;
        }
        this.edituserService.Update(this.editform.value, this.queryId).subscribe(data => {
            if (data) {
                this.toastr.success('Success', 'Updated', {
                    closeButton: true,
                });
                this.router.navigate(['users']);
            }
        },
            error => {
                console.log('Error', error);
            });
    }

    cancel() {
        this.router.navigate(['users']);
    }

    hideEye() {
        this.show = !this.show;
    }
}
