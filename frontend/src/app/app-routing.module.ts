import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
{ path: '', component: HomeComponent, pathMatch: 'full'} ,
{ path : 'user/create', loadChildren: () => import('./createuser/createuser.module').then(m => m.CreateuserModule)} , 
{ path : 'user', loadChildren: () => import('./edituser/edituser.module').then(m => m.EdituserModule)} , 
{ path : 'users', loadChildren: () => import('./getallusers/getallusers.module').then(m => m.GetallusersModule)} , 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
