import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Customers {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public firstName: string;

    @Column()
    public lastName: string;

    @Column()
    public phone: string;

    @Column()
    public email: string;

    @Column()
    public password: string;

}

export default Customers;
