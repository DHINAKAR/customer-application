import { createConnection } from "typeorm";
import Customers from "../entity/Customers";

export const connection = createConnection({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "root",
    database: "Customer",
    entities: [
        Customers
    ],
    synchronize: true,
    logging: false
});
