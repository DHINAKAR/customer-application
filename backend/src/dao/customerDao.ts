import { connection } from "../connection/DBConnection";
import Customers from "../entity/Customers";

class CustomerDao {

    public async getAllCustomers() {
        return await connection
            .then(async connection => {
                const customers: Customers[] = await connection.manager.find(Customers);
                return customers;
            })
            .catch(error => {
                return error;
            });
    }

    public async getCustomerById(id: number) {
        return await connection
            .then(async connection => {
                return await connection.manager.findOne(Customers, id);
            })
            .catch(async error => {
                return error;
            });
    }

    public async addCustomer(requestCustomer: Customers) {
        return await connection
            .then(async connection => {
                let customer = new Customers();
                customer.firstName = requestCustomer.firstName;
                customer.lastName = requestCustomer.lastName;
                customer.phone = requestCustomer.phone;
                customer.email = requestCustomer.email;
                customer.password = requestCustomer.password;
                return await connection.manager.save(customer);
            })
            .catch(async error => {
                return error;
            });
    }


    public async updateCustomer(id: number, requestCustomer: Customers) {
        return await connection
            .then(async connection => {
                let customer = await connection.manager.findOne(Customers, id);
                if (!customer) {
                    return { message: "No customer exist in the system" };
                }
                customer.firstName = requestCustomer.firstName;
                customer.lastName = requestCustomer.lastName;
                customer.phone = requestCustomer.phone;
                customer.email = requestCustomer.email;
                customer.password = requestCustomer.password;
                return await connection.manager.save(customer);
            })
            .catch(async error => {
                return error;
            });
    }

    public async deleteCustomer(id: number) {
        return await connection
            .then(async connection => {
                let customer = await connection.manager.findOne(Customers, id);
                if (!customer) {
                    return { message: "No customer exist in the system" };
                }
                return await connection.manager.remove(customer);
            })
            .catch(error => {
                return error;
            })
    }

}

export { CustomerDao }