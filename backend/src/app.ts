import * as express from 'express'
import { Routes } from "./routes/Routes";
import bodyParser = require("body-parser");
import { CustomerController } from './controller/customerController';
import { CustomerService } from './service/customerService';
import { CustomerDao } from './dao/customerDao';
const cors = require('cors')

var corsOptions = {
  origin: '*' // some legacy browsers (IE11, various SmartTVs) choke on 204 
}
class App {

    public app: express.Application;
    public route: Routes;

    constructor() {
        this.app = express();
        // support application/json type post data
        this.app.use(bodyParser.json());
        this.app.use(cors(corsOptions));

        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));

        this.route = new Routes(new CustomerController(new CustomerService(new CustomerDao)));
        this.route.routes(this.app);

    }

}

export default new App().app;
