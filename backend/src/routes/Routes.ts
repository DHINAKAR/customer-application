import { Request, Response } from "express";
import { CustomerController } from "../controller/customerController";

class Routes {


    constructor(private controller: CustomerController) {
    }

    public routes(app): void {

        app.get('/api/customer', (req: Request, res: Response) => {
            this.controller.getAllCustomers(req, res);
        });

        app.get('/api/customer/:id', (req: Request, res: Response) => {
            this.controller.getCustomerById(req, res);
        })

        app.post('/api/customer', (req: Request, res: Response) => {
            this.controller.addCustomer(req, res);
        })

        app.put('/api/customer/:id', (req: Request, res: Response) => {
            this.controller.updateCustomer(req, res);
        })

        app.delete('/api/customer/:id', (req: Request, res: Response) => {
            this.controller.deleteCustomer(req, res);
        })
    }
}

export { Routes };
