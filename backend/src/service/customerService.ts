import Customers from 'entity/Customers';
import { CustomerDao } from '../dao/customerDao';


class CustomerService {
    constructor(private dao: CustomerDao) {

    }
    public async getAllCustomers() {
        return await this.dao.getAllCustomers();
    }

    public async getCustomerById(id: number) {
        return await this.dao.getCustomerById(id);
    }

    public async addCustomer(customer : Customers) {
        return await this.dao.addCustomer(customer);
    }

    public async updateCustomer(id: number, customer:  Customers) {
        return await this.dao.updateCustomer(id, customer);
    }

    public async deleteCustomer(id : number) {
        return await this.dao.deleteCustomer(id);
    }
}

export { CustomerService }