import { Request, Response } from 'express';
import { CustomerService } from '../service/customerService';

export class CustomerController {

    constructor(private service: CustomerService) {
    }

    public async getAllCustomers(req: Request, res: Response) {
        const customer = await this.service.getAllCustomers();
        if (customer) {
            res.json(customer);
        }
    }

    public async getCustomerById(req: Request, res: Response) {
        const id = req.params.id;
        const customer = await this.service.getCustomerById(Number(id));
        if(customer.message){
            res.status(400);
            res.json(customer);
        }else {
            res.json(customer);
        }
    }

    public async addCustomer(req: Request, res: Response) {
        const customerObject = req.body;
        const customer = await this.service.addCustomer(customerObject);
        if (customer) {
            res.json(customer)
        }
    }

    public async updateCustomer(req: Request, res: Response) {
        const requestCustomer = req.body;
        const id = req.params.id;
        const customer = await this.service.updateCustomer(Number(id), requestCustomer);
        if(customer.message){
            res.status(400);
            res.json(customer);
        }else {
            res.json(customer);
        }
    }

    public async deleteCustomer(req: Request, res: Response) {
        const id = req.params.id;
        const customer = await this.service.deleteCustomer(Number(id));
        if(customer.message){
            res.status(400);
            res.json(customer);
        }else {
            res.json(customer);
        }
    }
}
